# pylint: disable=W0622, C0103
"""
    Module de persistence
"""
from user import User, Users


class Persistence:
    """
        Classe de persistence
    """
    def __init__(self, mongo):
        """
            Init de persistence
        """
        self.mongo = mongo

    def post_user(self, user):
        """
            post un user dans base mongo
        """
        json_user = user.to_json()
        self.mongo.db.users.insert_one(json_user)
        return user.id

    def put_users(self, users):
        """
            Put all users.
        """
        self.delete_users()
        self.mongo.db.users.insert_many(users.to_json())
        user_cursor = self.mongo.db.users.find()
        users_id = []
        for user in user_cursor:
            users_id.append(user['id'])
        return users_id

    def get_users(self):
        """
            Get all users
        """
        users_cursor = self.mongo.db.users.find()
        users = Users()
        for user in users_cursor:
            users.add_user(User(user['firstName'],
                                user['lastName'],
                                user['birthDay'],
                                user['position']['lat'],
                                user['position']['lon'],
                                id=user['id']))
        return users.to_json()

    def delete_users(self):
        """
            Delete all users
        """
        self.mongo.db.drop_collection("users")
        return True

    def delete_user(self, id):
        """
            Delete un user by id
        """
        delete_result = self.mongo.db.users.delete_one({"id": id})
        if delete_result.deleted_count <= 0:
            return False
        return True

    def get_user_by_id(self, id):
        """
            Get user by id
        """
        user = self.mongo.db.users.find_one({"id": id})
        if user is None:
            return False
        new_user = User(user['firstName'],
                        user['lastName'],
                        user['birthDay'],
                        user['position']['lat'],
                        user['position']['lon'],
                        id=user['id'])
        return new_user.to_json()

    def update_user_by_id(self, id, update):
        """
            Update user by id
        """
        result = self.mongo.db.users.update_one({"id": id}, {'$set': update})
        if result.modified_count <= 0:
            return False
        updated_user = self.get_user_by_id(id)
        return updated_user

    def get_users_by_page(self, page):
        """
            Get users by page
        """
        users_cursor = self.mongo.db.users.find()
        users = Users()
        for user in users_cursor.skip(page*100).limit(100):
            users.add_user(User(user['firstName'],
                                user['lastName'],
                                user['birthDay'],
                                user['position']['lat'],
                                user['position']['lon'],
                                id=user['id']))
        return users.to_json()
