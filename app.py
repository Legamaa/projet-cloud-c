# pylint: disable=W0702
"""
    Module de l'application
"""

import json
import os
from user import User, Users
from flask_pymongo import PyMongo
from flask import Flask, request, make_response
from persistence import Persistence




APP = Flask(__name__)
APP.config["MONGO_URI"] = os.environ.get('MONGODB_ADDON_URI')
PORT = int(os.environ.get('PORT'))
MONGO = PyMongo(APP)


@APP.route('/hello_world')
def hello_world():
    """
        Route de Hello World
    """
    return 'Welcome'


@APP.route("/user", methods=['POST'])
def add_user():
    """
        Route d'ajout de user
    """
    # Get Data
    user = User(request.json['firstName'],
                request.json['lastName'],
                request.json['birthDay'],
                request.json['position']['lat'],
                request.json['position']['lon'])

    # Add to DB
    persistence = Persistence(MONGO)
    user_id = persistence.post_user(user)
    user.id = str(user_id)

    reponse = make_response(json.dumps(user.to_json()))
    reponse.mimetype = "application/json"
    reponse.status_code = 201

    return reponse


@APP.route('/user', methods=['GET'])
def get_users():
    """
        Route de get all users, sur une page
    """
    page = request.args.get('page', 0)
    persistence = Persistence(MONGO)
    users = persistence.get_users_by_page(int(page))
    reponse = make_response(json.dumps(users))
    reponse.mimetype = "application/json"
    reponse.status_code = 200

    return reponse


@APP.route('/user', methods=['DELETE'])
def delete_users():
    """
        Route de delete all users
    """
    persistence = Persistence(MONGO)
    persistence.delete_users()
    return "OK", 200


@APP.route('/user/<iduser>', methods=['DELETE'])
def delete_user_by_id(iduser):
    """
        Route de delete un user by id
    """
    persistence = Persistence(MONGO)
    delete_result = persistence.delete_user(iduser)
    if delete_result:
        return "OK", 204
    return "BAD", 500


@APP.route('/user/<iduser>', methods=['GET'])
def get_user_by_id(iduser):
    """
        Route de get un user by id
    """
    persistence = Persistence(MONGO)
    user = persistence.get_user_by_id(iduser)
    if not user:
        return "NOT FOUND", 404
    reponse = make_response(json.dumps(user))
    reponse.mimetype = "application/json"
    reponse.status_code = 200

    return reponse


@APP.route('/user', methods=['PUT'])
def put_users():
    """
        Route put un user dans la base
    """
    persistence = Persistence(MONGO)
    users = Users()
    for user in request.json:
        new_user = User(user['firstName'],
                        user['lastName'],
                        user['birthDay'],
                        user['position']['lat'],
                        user['position']['lon'])
        users.add_user(new_user)

    ids = persistence.put_users(users)

    reponse = make_response(json.dumps(ids))
    reponse.mimetype = "application/json"
    reponse.status_code = 201

    return reponse


@APP.route('/user/<iduser>', methods=['PUT'])
def put_user_by_id(iduser):
    """
        Route de update d'un user
    """
    persistence = Persistence(MONGO)
    update = {}
    new_user = request.json

    try:
        update['firstName'] = new_user['firstName']
    except:
        print('No first Name')
    try:
        update['lastName'] = new_user['lastName']
    except:
        print('No last Name')
    try:
        update['position']['lat'] = new_user['position']['lat']
    except:
        print('No latitude')
    try:
        update['position']['lon'] = new_user['position']['lon']
    except:
        print('No longitude')

    result = persistence.update_user_by_id(iduser, update)
    if not result:
        reponse = make_response()
        reponse.status_code = 500


    reponse = make_response(json.dumps(result))
    reponse.mimetype = "application/json"
    reponse.status_code = 200

    return reponse


if __name__ == '__main__':
    APP.run(port=PORT)
