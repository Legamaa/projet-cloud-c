# pylint: disable=R0903, R0913, W0622, C0103
"""
    Module qui contient la classe user et users
"""

import random
from position import Position


class User:
    """
        Classe qui represente un objet user
    """

    def __init__(self, firstName, lastName, birthDay, lat, lon, id=False):
        """
        Init de la classe
        Verifie si un identifiant a ete donne ou pas en entree
        """

        if not id:
            self.id = str(random.randint(0, 2**30))
        else:
            self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.birthDay = birthDay
        self.position = Position(lat, lon)

    def to_json(self):
        """
            Renvoie un objet json pour un user en entree
        """

        return {
            "id": self.id,
            "firstName": self.firstName,
            "lastName": self.lastName,
            "birthDay": self.birthDay,
            "position": self.position.to_json()
        }


class Users:
    """
         Classe qui represente une collection d'objets user
    """

    def __init__(self):
        """
            Init de la classe
            Verifie si un identifiant a ete donne ou pas en entree
        """

        self.users = []

    def add_user(self, user):
        """
            Ajout d'un objet user dans la collection
        """
        self.users.append(user)

    def to_json(self):
        """
            Transforme la collection en json
        """
        return [user.to_json() for user in self.users]
