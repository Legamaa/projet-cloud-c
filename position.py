# pylint: disable=R0903
"""
    Module qui contient la classe position
"""
class Position:
    """
        Classe Position
    """
    def __init__(self, lat, lon):
        """
            Init de la classe
        """
        self.lat = lat
        self.lon = lon

    def to_json(self):
        """
            Cree un json des coordonnees
        """
        return {
            "lat": self.lat,
            "lon": self.lon
        }
